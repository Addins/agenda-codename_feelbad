Aplikasi ini dikembangkan dalam lingkungan sbb:

Sistem Operasi Windows 7
Dev C++ 4.9.9.2 dengan tambahan package CONIO 2.0 yang dapat diunduh dari devpaks.org

Untuk membuka source code Agenda Versa, klik ganda pada "Agendav1.dev"

Catatan:
CONIO 2.0 disertakan bersama dengan file ini.
Bila anda telah menginstall Dev C++, maka klik ganda pada file "conio-2.0-1mol.DevPak"
Maka package manager Dev C++ akan melakukan instalasi package tersebut untuk anda.