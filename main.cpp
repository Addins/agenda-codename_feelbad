/* 
 * File:   main.cpp
 * Author: addin
 *
 * Created on October 8, 2011, 12:20 PM
 */

//#define KIRI 1
#define TENGAH 2
//#define KANAN 3

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <ctime>
#include <string>
#include <constream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <windows.h>

using namespace std;
using namespace conio;
/*
 * 
 */
string hari[8]={"Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","-"};
//string bulan[13]={"Jan","Feb","Mar","Apr","Mei","Jun","Jul","Ags","Sep","Okt","Nov","Des","-"};

struct act {
    int id;
    tm waktu;
    string tempat;
    string deskripsi;
    int status_ulang; //0=once,1=daily,2=weekly,3=monthly,4=annually
    int status_alert; //0=not yet passed, 1=already passed
    act *prev,*next;
};

text_info info;
unsigned char x,y;
int indx; //jumlah data
//int sort_status; //0-9

act *root;
act *ujung;
//act *rootofsearch;
tm *now;
time_t now_sec;
char now_str[25];

ifstream inp ;
ofstream outp;

void aboutme();
void footer();
void header(string s,int y,int pos);
void exec(char &j);
void tampil();
int hapus();
int tambah(act *newent);
void menu();
bool IsEmpty();
act* inputKBoard(); 
void firstLoad();
act* string2act(const string &j,const char &delimi);
void act2file();
string int2string(int n);
bool findsubstr(string a, string b);
void cari(string s);
int actcpy(act &a,act &b);
int tmcmp(tm tm1, tm tm2);
int actedit(int ID);
int sort();
int keluar();
void bantuan();
int tombol();

//fungsi main

int main() {
    now_sec=time(NULL);
    now=localtime(&now_sec);
    firstLoad();
    char k;
    
    gettextinfo(&info);
    
    
    do {
        clrscr();
        
        header("Agenda Versa",2,TENGAH);
        //header("Kuartal4 di 2011",3,TENGAH);
        cout<<endl<<endl;
        now_sec=time(NULL);
        now=localtime(&now_sec);
        sprintf(now_str,"%s, %d/%d/%d %d:%d:%d",hari[now->tm_wday].c_str(),now->tm_mday,now->tm_mon+1,now->tm_year+1900,now->tm_hour,now->tm_min,now->tm_sec);
        cputsxy(52,3,now_str);gotoxy(1,6);
        sort();
        tampil();
        footer();
        gotoxy(1,18);
        k=tolower(tombol());
        exec(k);//Sleep(1000);
        }
        while (true);
        
    return EXIT_SUCCESS;
}
//akhir funsi main



bool IsEmpty() 
{
     return(root==NULL);
}

string int2string(int n)
{
       stringstream s;
       s<<n;
       return s.str();
}

bool findsubstr(string a, string b)
{
     if (a.find(b)>a.length()||a.find(b)<0)return false;
     else return true;
}

void cari(string s)
{
     string record,tanggal,jam;
     int count=0;
     char df[25];
     
     act *walkthrough;
     bool ketemu=false,pertama=true;
     cout<<setattr(WHITE)<<setbk(BLACK);
     if (!IsEmpty())
     {
                    walkthrough=root;
                    while (walkthrough!=NULL)
                    {
                          tanggal=hari[walkthrough->waktu.tm_wday]+","+int2string(walkthrough->waktu.tm_mday)+'/'+int2string(walkthrough->waktu.tm_mon+1);
                          tanggal+='/'+int2string(walkthrough->waktu.tm_year+1900);
                          jam=int2string(walkthrough->waktu.tm_hour)+':'+int2string(walkthrough->waktu.tm_min);
                          jam+=':'+int2string(walkthrough->waktu.tm_sec);
                          record=int2string(walkthrough->id)+"   "+tanggal+"\t"+jam+"\t\t"+walkthrough->tempat+"\t\t"+walkthrough->deskripsi;
                          if (findsubstr(record, s)){
                                                 ketemu=true;
                                                 count++;
                                                 
                                                 if (pertama){clrscr();header("Agenda Versa",2,TENGAH);
                                                 //header("Kuartal4 di 2011",3,TENGAH);
                                                 cout<<endl<<endl;
                                                 
                                                 cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<"ID   TANGGAL\t\tJAM\t\tTEMPAT\t\tDESKRIPSI\t\t\n";
                                                 pertama=false;gotoxy(1,7);}
                                                 
                                                 cout<<setattr(WHITE)<<setbk(BLACK)<<record<<endl;
                          }//langsung ditampilkan
                          walkthrough=walkthrough->next;
                    }
     }
     else cout<<"\nTidak ada kegiatan -_-\"\n";
     if (ketemu==false)
     {
                       clrscr();
                       int x=(80-(30+s.length()))/2;
                       gotoxy(x,10);
                      cout<<setattr(WHITE)<<setbk(BLUE)<<char(201);
                      for(int i=0;i<32+s.length();i++)cout<<char(205);cout<<char(187);
                      gotoxy(x,11);cout<<char(186)
                      <<setattr(LIGHTRED)<<setbk(BLUE)<<" kata kunci \""<<s<<"\" tidak ditemukan! "
                      <<setattr(WHITE)<<setbk(BLUE)<<char(186);
                      gotoxy(x,12);cout<<setattr(WHITE)<<setbk(BLUE)<<char(200);
                      for(int i=0;i<32+s.length();i++)cout<<char(205);cout<<char(188);
                      gotoxy(0,11);
     }
     else {sprintf(df,"%d Hasil Pencarian \"%s\"",count,s.c_str());cputsxy(50,2,df);gotoxy(1,6);}
}

void tampil()
{
     string tanggal, jam, status;
     int count=0;
     cout<<setiosflags(ios::left)<<setattr(WHITE)<<setbk(LIGHTBLUE);
     cout<<setw(2)<<"ID"<<setw(20)<<" TANGGAL "<<setw(9)<<" JAM "<<setw(18)
     <<" TEMPAT "<<setw(31)<<" DESKRIPSI "<<endl;fflush(stdout);//<<setw(14)<<" STATUS ULANG \n";
     act *walkthrough;
     if (!IsEmpty())
     {
                    walkthrough=root;
                    while (walkthrough!=NULL && count <15 )
                    {
                          if(walkthrough->status_alert==0)
                          {//cout<<setattr(DARKGRAY)<<setbk(BLACK);
                          //else 
                          cout<<setattr(WHITE)<<setbk(BLACK);
                          tanggal=hari[walkthrough->waktu.tm_wday]+","+int2string(walkthrough->waktu.tm_mday)+'/'+int2string(1+walkthrough->waktu.tm_mon);
                          tanggal+='/'+int2string(1900+walkthrough->waktu.tm_year);
                          jam=int2string(walkthrough->waktu.tm_hour)+':'+int2string(walkthrough->waktu.tm_min);
                          jam+=':'+int2string(walkthrough->waktu.tm_sec);
                          
                          cout<<setiosflags(ios::right);//<<setattr(WHITE);
                          cout<<setw(2)<<walkthrough->id<<setw(20)
                          <<tanggal<<setw(9)
                          <<jam<<resetiosflags(ios::left);
                          cout<<setw(18)
                          <<walkthrough->tempat<<setw(30)
                          <<walkthrough->deskripsi<<endl;
                          //<<walkthrough->status_ulang<<endl;
                          //cout<<"ID\t"<<walkthrough->id<<endl;
                          
                          /*switch (walkthrough->status_ulang)
                          {
                                 case 0:status=" sekali";break;
                                 case 1:status=" setiap hari";break;
                                 case 2:status=" setiap minggu";break;
                                 case 3:status=" setiap bulan";break;
                                 case 4:status=" setiap tahun";break;
                          }*/
                          //cout<<setw(7)<<walkthrough->status_alert<<endl;
                          count++;
                          }
                          walkthrough=walkthrough->next;
                    }
                    
                    walkthrough=ujung;
                    while (walkthrough!=NULL && count <15 )
                    {
                          if(walkthrough->status_alert==1)
                          {cout<<setattr(DARKGRAY)<<setbk(BLACK);
                          //else cout<<setattr(WHITE)<<setbk(BLACK);
                          tanggal=hari[walkthrough->waktu.tm_wday]+","+int2string(walkthrough->waktu.tm_mday)+'/'+int2string(1+walkthrough->waktu.tm_mon);
                          tanggal+='/'+int2string(1900+walkthrough->waktu.tm_year);
                          jam=int2string(walkthrough->waktu.tm_hour)+':'+int2string(walkthrough->waktu.tm_min);
                          jam+=':'+int2string(walkthrough->waktu.tm_sec);
                          
                          cout<<setiosflags(ios::right);//<<setattr(WHITE);
                          cout<<setw(2)<<walkthrough->id<<setw(20)
                          <<tanggal<<setw(9)
                          <<jam<<resetiosflags(ios::left);
                          cout<<setw(18)
                          <<walkthrough->tempat<<setw(30)
                          <<walkthrough->deskripsi<<endl;
                          count++;
                          }
                          walkthrough=walkthrough->prev;
                    }
     }
     else cout<<"\nTidak ada kegiatan -_-\"\n";
}

void act2file()
{
     act *selecta;
     selecta=root;
    outp.open("data.csv",ios::out|ios::trunc);
    while (selecta!=NULL)
    {
    outp//<<selected->id<<","
    <<selecta->waktu.tm_mday<<"/"<<int(selecta->waktu.tm_mon+1)<<"/"<<int(selecta->waktu.tm_year+1900)<<";"
    <<selecta->waktu.tm_hour<<":"<<selecta->waktu.tm_min<<":"<<selecta->waktu.tm_sec<<";"
    <<selecta->tempat<<";"<<selecta->deskripsi<<";"<<selecta->status_ulang<<endl;
    selecta=selecta->next;
    }
    outp.close();
    cout<<setattr(LIGHTBLUE)<<"Sudah tersimpan!\n"<<setattr(WHITE);
    getch();
}

int hapus()
{
    char pil;
    string op;
    gotoxy(10,10);
    cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(201);
    for(int i=0;i<57;i++)cout<<char(205);cout<<char(187);
    gotoxy(10,11);cout<<char(186)<<" Masukan  ID yang akan dihapus atau 'all' untuk semua!   "<<char(186);
    gotoxy(10,12);cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(200);
    for(int i=0;i<57;i++)cout<<char(205);cout<<char(188);
    gotoxy(65,11);fflush(stdin);
    //cout<<"Masukan  ID yang akan dihapus atau 'all' untuk semua! ";
    getline(cin,op);
    cout<<setattr(WHITE)<<setbk(BLACK);
    act *through,*temp;
    through=ujung;
    bool ketemu=false;
    if (op!="all")
    {
                  int ops=atoi(op.c_str());
                  //cout<<ops<<endl;
                  for (;through!=NULL;)
                  {
                      temp=through;
                      through=through->prev;
                      if (temp->id==ops)
                      {
                                        cout<<setattr(WHITE)<<setbk(BLACK);
                                        clrscr();fflush(stdin);
                                        gotoxy(20,11);cout<<" Yakin ingin menghapus data dengan ID  "<<op<<" ? ";
                                        gotoxy(63,11);
         
                                        //cout<<"Yakin ingin menghapus data dengan ID "<<op<<" ? ";
                                        cin>>pil;
                                        fflush(stdin);
                                        if (pil!='y')return 0;         
                                        if (temp!=root){
                                           ketemu=true;
                                           act *depan, *belakang;
                                           depan=temp->next;
                                           belakang=temp->prev;
                                           if (depan!=NULL)depan->prev=belakang;
                                           if (belakang!=NULL)belakang->next=depan;
                                           delete temp;}
                                        else if (op=="0")
                                        {
                                            ketemu=true;
                                            if(temp->next!=NULL)
                                            {
                                                                root=temp->next;
                                                                root->prev=NULL;
                                            }
                                            else root=NULL;
                                            
                                            delete temp;
                                        } 
                      }
                  }
                  if (ketemu){cout<<"Data kegiatan dengan ID "<<ops<<" telah dihapus...\n";act2file();}
                  else cout<<"Data kegiatan dengan ID \""<<op<<"\" tidak ditemukan...\n";
    }
    else if (op=="all") 
    {
         clrscr();
         cout<<setattr(WHITE)<<setbk(BLACK);
         gotoxy(20,10);
         cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(201);
         for(int i=0;i<36;i++)cout<<char(205);cout<<char(187);fflush(stdin);
         gotoxy(20,11);cout<<char(186)<<" Yakin ingin menghapus semua data?  "<<char(186);
         gotoxy(20,12);cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(200);
         for(int i=0;i<36;i++)cout<<char(205);cout<<char(188);
         gotoxy(55,11);
         //cout<<"Yakin ingin menghapus semua data? ";
         cin>>pil;
         cout<<setattr(WHITE)<<setbk(BLACK);
         fflush(stdin);
         if (pil!='y')return 0;
         for (;through!=NULL;)
         {
             temp=through;
             through=through->prev;
             delete temp;
         }
         root=NULL;
         cout<<"Semua data kegiatan telah dihapus...\n";
         act2file();         
    }
    else cout<<"Data kegiatan dengan ID \""<<op<<"\" tidak ditemukan...\n";
    //getch();
    return 1;
}

int tambah(act *newent)
{
    newent->id=indx++;
    if (IsEmpty())
    {
    newent->prev=NULL;
    newent->next=NULL;
    root=newent;
    ujung=newent;
    //return 1;
    }
    else
    {
        ujung->next=newent;
        newent->prev=ujung;
        newent->next=NULL;
        ujung=newent;
        //return 1;
    }
        
    return 1;
}

act* inputKBoard()
{
    act *newent;
    newent=new act;
    newent->waktu.tm_sec=0;
    newent->waktu.tm_hour=12;
    newent->waktu.tm_min=59;
    ulang:
    clrscr();
    header("Agenda Versa",2,TENGAH);
    //header("Kuartal4 di 2011",3,TENGAH);
    cout<<endl<<endl;
    now_sec=time(NULL);
    now=localtime(&now_sec);
    sprintf(now_str,"%s, %d/%d/%d %d:%d:%d",hari[now->tm_wday].c_str(),now->tm_mday,now->tm_mon+1,now->tm_year+1900,now->tm_hour,now->tm_min,now->tm_sec);
    cputsxy(52,3,now_str);gotoxy(1,6);
    cout<<"\tTanggal tgl <spasi> bln <spasi> thn : ";
    cin>>newent->waktu.tm_mday>>newent->waktu.tm_mon>>newent->waktu.tm_year;    
    newent->waktu.tm_mon-=1;
    newent->waktu.tm_year-=1900;
    if (mktime(&newent->waktu)==-1){clrscr();cout<<setattr(RED)<<"Tanggal salah!"<<setattr(WHITE)<<"\nBuat baru!"<<endl;goto ulang;}
    cout<<"\tJam jam <spasi> mnt : ";
    cin>>newent->waktu.tm_hour>>newent->waktu.tm_min;
    if (tmcmp(*now,newent->waktu)==2)newent->status_alert=0;
    else {newent->status_alert=1;cout<<"Waktu yang anda masukan sudah lewat!\n";goto ulang;}
    cout<<"\tTempat: ";
    fflush(stdin);
    getline(cin,newent->tempat);
    cout<<"\tdeskripsi: ";
    getline(cin,newent->deskripsi);
    //cout<<"Diulang? (0=sekali,1=setiap hari,2=setiap minggu,3=setiap bulan,4=setiap tahun) ";
    //cin>>newent->status_ulang;

    newent->status_ulang=0;
    return newent; 
 }

void menu()
{
        cout<<" "<<setattr(LIGHTBLUE)<<setbk(BLACK);
        for(int i=0;i<16;i++)cout<<char(220);
        cout<<setattr(WHITE)<<setbk(BLACK)
        <<"\n "<<setattr(LIGHTRED)<<setbk(LIGHTBLUE)<<" B"<<setattr(WHITE)<<setbk(LIGHTBLUE)<<"uat kegiatan  "
        <<setattr(WHITE)<<setbk(BLACK)<<"\n "<<setattr(LIGHTRED)<<setbk(LIGHTBLUE)<<" C"<<setattr(WHITE)<<setbk(LIGHTBLUE)<<"ari Kegiatan  "
        <<setattr(WHITE)<<setbk(BLACK)<<"\n "<<setattr(LIGHTRED)<<setbk(LIGHTBLUE)<<" U"<<setattr(WHITE)<<setbk(LIGHTBLUE)<<"bah kegiatan  "
        <<setattr(WHITE)<<setbk(BLACK)<<"\n "<<setattr(LIGHTRED)<<setbk(LIGHTBLUE)<<" H"<<setattr(WHITE)<<setbk(LIGHTBLUE)<<"apus kegiatan ";
        cout<<setattr(WHITE)<<setbk(BLACK);
        cout<<"\n "<<setattr(LIGHTBLUE)<<setbk(BLACK);
        for(int i=0;i<16;i++)cout<<char(219);
        cout<<setbk(WHITE)<<"\n "<<setattr(WHITE)<<setbk(BLUE)<<"  Menu  ";
        cout<<setattr(WHITE)<<setbk(BLACK);
        //<<"\nB"<<"antuan"
        //<<"\nK"<<"eluar"
        //<<"\nA"<<"bout me ^_^";
        //<<endl;
        char g = tolower(tombol());
        string keyword;
        int no;
        switch (g)
        {
            case 'b':tambah(inputKBoard());act2file();break;
            case 'c': gotoxy(20,10);
                      cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(201);
                      for(int i=0;i<40;i++)cout<<char(205);cout<<char(187);
                      gotoxy(20,11);cout<<char(186)<<" Cari? (case sensitive)                 "<<char(186);
                      gotoxy(20,12);cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(200);
                      for(int i=0;i<40;i++)cout<<char(205);cout<<char(188);
                      gotoxy(45,11);
                      getline(cin,keyword);cari(keyword);
                      cout<<setattr(WHITE)<<setbk(BLACK);tombol();break;
            case 'k':keluar();break;//keluar
            case 'h':hapus();break;
            case 'u':gotoxy(20,10);
                      cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(201);
                      for(int i=0;i<43;i++)cout<<char(205);cout<<char(187);
                      gotoxy(20,11);cout<<char(186)<<" Masukan ID kegiatan yang ingin diubah :   "<<char(186);
                      gotoxy(20,12);cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(200);
                      for(int i=0;i<43;i++)cout<<char(205);cout<<char(188);
                      gotoxy(61,11);
                      cin>>no;
                      if (actedit(no))act2file();cout<<setattr(WHITE)<<setbk(BLACK);break;
            case 'a':aboutme();break;
        }
}

void exec(char &j)
{
     //system("clear");
     string keyword;
     int no;
     switch (j)
     {
            case 'b':tambah(inputKBoard());act2file();break;
            case 'c': gotoxy(20,10);
                      cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(201);
                      for(int i=0;i<40;i++)cout<<char(205);cout<<char(187);fflush(stdin);
                      gotoxy(20,11);cout<<char(186)<<" Cari? (case sensitive)                 "<<char(186);
                      gotoxy(20,12);cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(200);
                      for(int i=0;i<40;i++)cout<<char(205);cout<<char(188);
                      gotoxy(45,11);
                      getline(cin,keyword);cari(keyword);
                      cout<<setattr(WHITE)<<setbk(BLACK);tombol();break;
            case 'n':bantuan();break;//keluar
            case 'h':hapus();break;
            case 'u':gotoxy(20,10);
                      cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(201);
                      for(int i=0;i<43;i++)cout<<char(205);cout<<char(187);fflush(stdin);
                      gotoxy(20,11);cout<<char(186)<<" Masukan ID kegiatan yang ingin diubah :   "<<char(186);
                      gotoxy(20,12);cout<<setattr(WHITE)<<setbk(LIGHTBLUE)<<char(200);
                      for(int i=0;i<43;i++)cout<<char(205);cout<<char(188);
                      gotoxy(61,11);
                      cin>>no;
                      if (actedit(no))act2file();cout<<setattr(WHITE)<<setbk(BLACK);break;
            case 'm':menu();cout<<setattr(WHITE)<<setbk(BLACK);break;
            case 'k':keluar();break;//keluar
            case 'a':aboutme();break;
     }
}

void firstLoad()
{
     inp.open("data.csv",ios::in);
     string buff;
     if (inp.good())
     {
                    while (!inp.eof())
                    {
                          getline(inp,buff);//baca file per baris
                          if (buff!="\0")tambah(string2act(buff,';'));//dipecah dengan pembatas koma
                    }
    }
    else cout<<"File not goood"<<endl; 
}
      
act* string2act(const string &j,const char &delimi)
{
       act *ent,*n;
       ent= new act;
       
       string temp[5];//buat nampung pecahan dari buff
       string tgl[3],jam[3];
       
       int l=0;
       //telusur per karakter
       for (int i=0;i<j.length();i++)
       {
           if (j[i]!=delimi && i!=j.length())
           temp[l]+=j[i];
           else l++;continue;
       }
       l=0;
       for (int i=0;i<temp[0].length();i++)
       {
           if (temp[0][i]!='/' && i!=temp[0].length())
           tgl[l]+=temp[0][i];
           else l++;continue;
       }
       l=0;
       for (int i=0;i<temp[1].length();i++)
       {
           if (temp[1][i]!=':' && i!=temp[1].length())
           jam[l]+=temp[1][i];
           else l++;continue;
       }
       ent->waktu.tm_hour=atoi(jam[0].c_str());
       ent->waktu.tm_min=atoi(jam[1].c_str());
       ent->waktu.tm_sec=atoi(jam[2].c_str());
       ent->waktu.tm_mday=atoi(tgl[0].c_str());
       ent->waktu.tm_mon=atoi(tgl[1].c_str())-1;
       ent->waktu.tm_year=atoi(tgl[2].c_str())-1900;
       if (mktime(&ent->waktu)==-1)ent->waktu.tm_wday=7;
       
       if (tmcmp(*now,ent->waktu)==2)ent->status_alert=0;
       else ent->status_alert=1;
       
       ent->tempat=temp[2];
       ent->deskripsi=temp[3];
       ent->status_ulang=atoi(temp[4].c_str());
       
       return ent;
}

int actcpy(act *a,act *b)
{
    a->id=b->id;
    a->waktu=b->waktu;
    a->tempat=b->tempat;
    a->deskripsi=b->deskripsi;
    a->status_ulang=b->status_ulang;
    
}

void header(string s,int y, int pos)
{
     //textcolor(  );
     //textbackground( LIGHTBLUE );
     highvideo();
     if (pos=TENGAH)
     {
                    cout<<setattr(YELLOW)<<setxy((80/2-(s.length()/2)),y)<<s<<endl;
                    cout<<setattr(WHITE);
     }
}     

void footer()
{
     //textcolor(  );
     //textbackground( LIGHTBLUE );
     gotoxy(1,24);
     highvideo();
     cout<<setattr(LIGHTRED)<<setbk(WHITE)
        <<"   M"<<setattr(BLACK)<<setbk(WHITE)<<"enu        \t"
        <<setattr(BLACK)<<setbk(WHITE)<<"Ba"
        <<setattr(LIGHTRED)<<setbk(WHITE)<<"n"<<setattr(BLACK)<<setbk(WHITE)<<"tuan        \t"
        <<setattr(LIGHTRED)<<setbk(WHITE)<<"K"<<setattr(BLACK)<<setbk(WHITE)<<"eluar         \t"
        <<setattr(LIGHTRED)<<setbk(WHITE)<<"A"<<setattr(BLACK)<<setbk(WHITE)<<"bout me\t\t        "
        <<setattr(WHITE)<<setbk(BLACK);
        //clreol();delline();delline();delline();
}     

int tmcmp( const tm tm1, const tm tm2)
{
    /*jika tm1 lebih besar dari pada tm2 maka return 1, jika sebaliknya return 2.
    jika tm1==tm2 maka return 0*/
    if (tm1.tm_year>tm2.tm_year)return 1;
    else if (tm1.tm_year<tm2.tm_year)return 2;
    else if (tm1.tm_mon>tm2.tm_mon)return 1;
    else if (tm1.tm_mon<tm2.tm_mon)return 2;
    else if (tm1.tm_mday>tm2.tm_mday)return 1;
    else if (tm1.tm_mday<tm2.tm_mday)return 2;
    else if (tm1.tm_hour>tm2.tm_hour)return 1;
    else if (tm1.tm_hour<tm2.tm_hour)return 2;
    else if (tm1.tm_min>tm2.tm_min)return 1;
    else if (tm1.tm_min<tm2.tm_min)return 2;
    else if (tm1.tm_sec>tm2.tm_sec)return 1;
    else if (tm1.tm_sec<tm2.tm_sec)return 2;
    else return 0;
}

int actedit(int ID)
{
     char pil;
     act *walkthrough;
     cout<<setattr(WHITE)<<setbk(BLACK);
     if (!IsEmpty())
     {
                    walkthrough=root;
                    while (walkthrough!=NULL)
                    {
                          if (walkthrough->id==ID)
                          {
                          clrscr();
                          header("Agenda Versa",2,TENGAH);
                          //header("Kuartal4 di 2011",3,TENGAH);cout<<endl<<endl;
                          cout<<"ID : "<<walkthrough->id<<endl;
                          cout<<"\nData sekarang: \nTanggal\t\t: "<<hari[walkthrough->waktu.tm_wday]<<", "
                          <<walkthrough->waktu.tm_mday<<"/"<<walkthrough->waktu.tm_mon+1<<"/"<<walkthrough->waktu.tm_year+1900<<endl
                          <<"Jam\t\t: "<<walkthrough->waktu.tm_hour<<":"<<walkthrough->waktu.tm_min<<endl
                          <<"Tempat\t\t: "<<walkthrough->tempat<<endl<<"Deskripsi\t: "<<walkthrough->deskripsi<<endl;
                          cout<<"\nYakin ingin mengubah data tersebut? ";
                          cin>>pil;fflush(stdin);
                          if (pil!='y'){cout<<"Data tidak diubah...\n";return 0;}
                          cout<<"\nData baru: \n";
                          walkthrough->waktu.tm_sec=0;
                          walkthrough->waktu.tm_hour=12;
                          walkthrough->waktu.tm_min=59;
                          ulang:
                          cout<<"Tanggal tgl <spasi> bln <spasi> thn : ";
                          cin>>walkthrough->waktu.tm_mday>>walkthrough->waktu.tm_mon>>walkthrough->waktu.tm_year;    
                          walkthrough->waktu.tm_mon-=1;
                          walkthrough->waktu.tm_year-=1900;
                          if (mktime(&walkthrough->waktu)==-1){clrscr();cout<<setattr(RED)<<"Tanggal salah!"<<setattr(WHITE)<<"\nBuat baru!"<<endl;goto ulang;}
                          cout<<"Jam jam <spasi> mnt : ";
                          cin>>walkthrough->waktu.tm_hour>>walkthrough->waktu.tm_min;
                          if (tmcmp(*now,walkthrough->waktu)==2)walkthrough->status_alert=0;
                          else {walkthrough->status_alert=1;cout<<"Waktu yang anda masukan sudah lewat!\n";goto ulang;}
                          cout<<"Tempat: ";
                          fflush(stdin);
                          getline(cin,walkthrough->tempat);
                          cout<<"deskripsi: ";
                          getline(cin,walkthrough->deskripsi);
                          walkthrough->status_ulang=0;
                          cout<<"Data telah diubah ^_^\n";
                          tombol();
                          break;  
                          }
                          walkthrough=walkthrough->next;
                    }
     return 1;}
     else cout<<"\nTidak ada kegiatan -_-\"\n";
}

int sort()
{
    act *walk,*temp;
    act *depan, *belakang;
    depan=belakang=temp=walk=NULL;
    /*
    */
    int k=0,j=0,jum_dat=0;
    act *scd;
    walk=root;
    while (walk!=NULL)
    {
          jum_dat++;
          walk=walk->next;
    }
    //cout<<jum_dat;
    
    
    for (int i=0;i<jum_dat && jum_dat!=1;i++)
    {
        walk=root;
        for (int h=0;h<jum_dat;h++)
        {
            scd=walk->next;
            //cout<<root<<"_"<<scd;tombol();
            if (tmcmp(*now,walk->waktu)==2)walk->status_alert=0;
            else walk->status_alert=1;
            if(scd!=NULL && walk!=NULL){
            if(tmcmp(walk->waktu,scd->waktu)==1)
            {
             temp=new act;
             actcpy(temp,walk);
             actcpy(walk,scd);
             actcpy(scd,temp);
             delete temp;
             
             }}
            walk=walk->next;
        }
    }
    
}

void aboutme()
{
     //clrscr();
     gotoxy(15,10);
     cout<<setattr(WHITE)<<setbk(BLUE)<<char(201);
     for(int i=0;i<50;i++)cout<<char(205);cout<<char(187);
     gotoxy(15,11);cout<<char(186);for(int i=0;i<50;i++)cout<<" ";cout<<char(186);
     gotoxy(15,12);cout<<char(186);for(int i=0;i<50;i++)cout<<" ";cout<<char(186);
     gotoxy(15,13);cout<<char(186)<<"                   Agenda Versa                   "<<char(186);
     gotoxy(15,14);cout<<char(186);for(int i=0;i<50;i++)cout<<" ";cout<<char(186);
     gotoxy(15,15);cout<<char(186)<<"      dibuat pada kuartal keempat tahun 2011      "<<char(186);
     gotoxy(15,16);cout<<char(186);for(int i=0;i<50;i++)cout<<" ";cout<<char(186);
     gotoxy(15,17);cout<<char(186)<<"                       Oleh                       "<<char(186);
     gotoxy(15,18);cout<<char(186)<<"                  Addinul Kintangi                "<<char(186);
     gotoxy(15,19);cout<<char(186)<<"                   1110091000054                  "<<char(186);
     gotoxy(15,20);cout<<char(186);for(int i=0;i<50;i++)cout<<" ";cout<<char(186);
     gotoxy(15,21);cout<<char(186)<<" Terima Kasih telah menjalankan program ini.. "<<char(1)<<" "<<char(2)<<" "<<char(186);
     gotoxy(15,22);cout<<char(186);for(int i=0;i<50;i++)cout<<" ";cout<<char(186);
     gotoxy(15,23);cout<<char(200);for(int i=0;i<50;i++)cout<<char(205);cout<<char(188);
     cout<<setattr(WHITE)<<setbk(BLACK);
     tombol();
}

void bantuan()
{
     //clrscr();
     int a=2,b=10;
     gotoxy(b,a++);
     cout<<setattr(WHITE)<<setbk(BLUE)<<char(201);
     for(int i=0;i<60;i++)cout<<char(205);cout<<char(187);
     gotoxy(b,a++);cout<<char(186);for(int i=0;i<60;i++)cout<<" ";cout<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"      Tekan 'b' untuk membuat kegiatan baru.                ";cout<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"      Tekan 'u' untuk mengubah kegiatan yang sudah ada      "<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"                dengan menggunakan ID untuk menentukan      ";cout<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"                kegiatan yang akan diubah.                  "<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"      Tekan 'h' untuk menghapus kegiatan dengan ID atau     ";cout<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"                'all' untuk menghapus semua kegiatan.       "<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"      Tekan 'c' untuk mencari kegiatan.                     "<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"      Tekan 'k' untuk menutup aplikasi.                     "<<char(186);
     gotoxy(b,a++);cout<<char(186);for(int i=0;i<60;i++)cout<<" ";cout<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"  Untuk buat kegiatan baru, gunakan angka dan pisah dengan  "<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"  spasi, contoh:                                            "<<char(186);
     gotoxy(b,a++);cout<<char(186);for(int i=0;i<60;i++)cout<<" ";cout<<char(186);
     gotoxy(b,a++);cout<<char(186)
     <<setattr(YELLOW)<<setbk(BLUE)<<"     Tanggal tgl <spasi> bln <spasi> thn : 1 1 2012         "<<setattr(WHITE)<<setbk(BLUE)<<char(186);
     gotoxy(b,a++);cout<<char(186);for(int i=0;i<60;i++)cout<<" ";cout<<char(186);
     gotoxy(b,a++);cout<<char(186)<<"  Untuk 1 januari 2012.                                     "<<char(186);
     gotoxy(b,a++);cout<<char(204);for(int i=0;i<60;i++)cout<<char(205);cout<<char(185);
     gotoxy(b,a++);cout<<char(186)<<"  Terima Kasih telah menjalankan program ini... "<<char(1)<<" "<<char(2)<<"         "<<char(186);
     gotoxy(b,a++);cout<<char(200);for(int i=0;i<60;i++)cout<<char(205);cout<<char(188);
     cout<<setattr(WHITE)<<setbk(BLACK);
     tombol();
}

int keluar()
{
    char h;
    gotoxy(25,10);
    cout<<setattr(RED)<<setbk(WHITE)<<char(201);
    for(int i=0;i<30;i++)cout<<char(205);cout<<char(187);
    gotoxy(25,11);cout<<char(186);for(int i=0;i<30;i++)cout<<" ";cout<<char(186);
    gotoxy(25,12);cout<<char(186)<<"   Yakin ingin keluar?        "<<char(186);
    gotoxy(25,13);cout<<char(186);for(int i=0;i<30;i++)cout<<" ";cout<<char(186);
    gotoxy(25,14);cout<<setattr(RED)<<setbk(WHITE)<<char(200);
    for(int i=0;i<30;i++)cout<<char(205);cout<<char(188);
    gotoxy(48,12);fflush(stdin);
    cin>>h;
    cout<<setattr(WHITE)<<setbk(BLACK);
    if (tolower(h)=='y')exit(0);
}

int tombol()
{
    int input;
    input = getch();
    if (input==224)
    {
                   input=getch();
                   switch(input)
                   {
                                case 72:return 1;
                                case 80:return 2;
                                case 75:return 3;
                                case 77:return 4;
                   }
    }
    else
    return input;
}
